% Read data from dataset and create i_k and v_k
[i, v] = readDataset("exercise2_data");

solutionA(v, i);

solutionB();

[y, phi] = solutionC(v, i);

inverse = solutionD(phi);

% solutionE(v, i, phi);

solutionF(v, i);