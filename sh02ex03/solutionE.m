function [  ] = solutionE( v, i, phi )

p = (pinv(phi)*i).';
x = linspace(1, 800);
y = p(1) + x * p(2) + power(x, 2) * p(3);
figure(1);
plot(x, y, '*');

% Alternatively: polyfit
% q = polyfit(v, i, 2)
% x1 = linspace(1, 800);
% y1 = polyval(q, x1);
% figure(1);
% plot(x1,y1);

end

