function [ i, v ] = readDataset( dataset_name )

dataset = matfile(dataset_name);
data = dataset.data;

i = data(:,2);
v = data(:,1);

end

