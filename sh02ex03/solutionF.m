function [  ] = solutionF( v, i )

i_new = i / 1000;
v_new = v / 1000;

phi = ones(size(i_new, 1), 3);
for n = 1:size(i_new, 1)
    phi(n, 2) = v_new(n);
    phi(n, 3) = v_new(n) * v_new(n);
end

p = (pinv(phi)*i_new).';
disp(p);
x = linspace(0,800);
y = p(1) + x * p(2) + power(x, 2) * p(3);
figure(1);
plot(x, y, '*');

end
