function [ inverse ] = solutionD( phi )

inverse = inv(phi.' * phi);

if det(phi.' * phi) == 0
    disp("Is not invertible");
else
    disp("Is invertible");
end

end

