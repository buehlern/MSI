function [ output_args ] = solutionA( v, i )

figure(1);
hold on;
plot(v, i, 'r*');
title('Exercise a): data plot (raw)', ...
'Interpreter', 'latex', 'FontSize', 14);
xlabel('Voltage / mV');
ylabel('Current / mA');
set(legend, 'Interpreter', 'latex', 'FontSize', 14);

end
