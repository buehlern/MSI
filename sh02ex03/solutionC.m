function [ y, phi ] = solutionC( v, i )

y = i;
phi = ones(size(i, 1), 3);
for n = 1:size(i, 1)
    phi(n, 2) = v(n);
    phi(n, 3) = v(n) * v(n);
end

end

