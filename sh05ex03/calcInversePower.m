function [ inverse_power ] = calcInversePower( time, volume )

% constant pressure
c_p = 4.19;
% change in temperature
delta_T = 100;
% density of water
ro = 1;

inverse_power = time / (c_p * delta_T * volume * ro);

end

