% Variables
litres = 1.0;

% Read in the file
times = readDataset("powerData.dat");

%% a)
% Calculate the inverse power
inverse_power = calcInversePower(times, litres);

%% b)

%% c)
% Calculate the LLS inverse power
P_hat = estimateInversePower(litres, inverse_power);

%% d)
% Calculate the variance
variance = calcVariance(P_hat, inverse_power);
