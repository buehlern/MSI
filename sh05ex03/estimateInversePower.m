function [ mean_power ] = estimateInversePower( litres, inverse_power )

theta = (inverse_power' * inverse_power) \ (inverse_power' * litres);

mean_power = theta(1) + theta(2) * litres;

end

