function [ variance ] = calcVariance( P_hat, inverse_power )

variance = sum((P_hat - inverse_power).^2);

end

