% Read data from dataset and create i_k and v_k
[i_k, v_k] = readDataset("exercise1_dataset");

% Get amount of measures and student number
n_measure = size(i_k, 1);
n_students = size(i_k, 2);

arr_SA = calcR_SA(i_k, v_k, n_measure, n_students);
arr_LS = calcR_LS(i_k, v_k, n_measure, n_students);
arr_EV = calcR_EV(i_k, v_k, n_measure, n_students);

% Solution exercise a)
solutionA(arr_SA, arr_LS, arr_EV, n_measure)

% Solution exercise b)
solutionB(arr_SA, arr_LS, arr_EV)

% Solution exercise c)
solutionC(arr_SA, arr_LS, arr_EV, n_measure)

% Solution exercise d)
solutionD(arr_SA, arr_LS, arr_EV, n_measure)