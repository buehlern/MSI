function [  ] = solutionC( arr_SA, arr_LS, arr_EV, n_measure )
% Calculate the mean over one measurement for all 200 students.

mean_SA = mean(arr_SA.');
mean_LS = mean(arr_LS.');
mean_EV = mean(arr_EV.');

figure(5);
hold on;
plot(1:n_measure, mean_SA);
plot(1:n_measure, mean_LS);
plot(1:n_measure, mean_EV);
title('Exercise c): mean over $\hat{R}_{*}(N)$', ...
'Interpreter', 'latex', 'FontSize', 14)
xlabel('Measurement')
ylabel('R / \Omega')
legend('$\hat{R}_{SA}$', '$\hat{R}_{LS}$', '$\hat{R}_{EV}$');
set(legend, 'Interpreter', 'latex', 'FontSize', 14);

end

