function [  ] = solutionD( arr_SA, arr_LS, arr_EV, n_measure )
% Plot the histogram for each estimator.

figure(6);
histogram(arr_SA(n_measure,:));
title('Exercise d): histogram over $\hat{R}_{SA}(N_{max})$', ...
'Interpreter', 'latex', 'FontSize', 14)
xlabel('Measurement')
ylabel('R / \Omega')

figure(7);
histogram(arr_LS(n_measure,:));
title('Exercise d): histogram over $\hat{R}_{LS}(N_{max})$', ...
'Interpreter', 'latex', 'FontSize', 14)
xlabel('Measurement')
ylabel('R / \Omega')

figure(8);
histogram(arr_EV(n_measure,:));
title('Exercise d): histogram over $\hat{R}_{EV}(N_{max})$', ...
'Interpreter', 'latex', 'FontSize', 14)
xlabel('Measurement')
ylabel('R / \Omega')

end

