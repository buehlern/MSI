function [ i_k, v_k ] = readDataset( dataset_name )

dataset = matfile(dataset_name);

i_k = dataset.i_k;
v_k = dataset.v_k;

end

