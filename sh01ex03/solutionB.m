function [  ] = solutionB( arr_SA, arr_LS, arr_EV )
% Calculate the resistance for all students and plot the results in one
% plot.

figure(2);
hold on;
plot(arr_SA);
title('Exercise b): multiple $R_{SA}$', ...
'Interpreter', 'latex', 'FontSize', 14)
xlabel('Measurement')
ylabel('R_{SA} / \Omega')

figure(3);
hold on;
plot(arr_LS);
title('Exercise b): multiple $R_{LS}$', ...
'Interpreter', 'latex', 'FontSize', 14)
xlabel('Measurement')
ylabel('R_{LS} / \Omega')

figure(4);
hold on;
plot(arr_EV);
title('Exercise b): multiple $R_{EV}$', ...
'Interpreter', 'latex', 'FontSize', 14)
xlabel('Measurement')
ylabel('R_{EV} / \Omega')

end

