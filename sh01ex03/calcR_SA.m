function [ arr ] = calcR_SA( i, u, n_measure, n_students )

arr = zeros(n_measure, n_students);

for n_stud = 1:n_students
    sum = 0;
    for n_meas = 1:n_measure
        sum = sum + (u(n_meas, n_stud) / i(n_meas, n_stud));
        arr(n_meas, n_stud) = sum / n_meas;
    end
end
    
end

