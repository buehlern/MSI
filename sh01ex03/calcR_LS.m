function [ arr ] = calcR_LS( i, u, n_measure, n_students )

num = 0;
den = 0;
arr = zeros(n_measure, n_students);

for n_stud = 1:n_students
    num = 0;
    den = 0;
    for n_meas = 1:n_measure
        num = num + u(n_meas, n_stud) * i(n_meas, n_stud);
        den = den + i(n_meas, n_stud) * i(n_meas, n_stud);
        arr(n_meas, n_stud) = (num / n_meas) / (den / n_meas);
    end
end
    
end



