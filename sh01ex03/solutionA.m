function [  ] = solutionA( arr_SA, arr_LS, arr_EV, n_measure )
% Calculate the resistance for the first student.

figure(1);
hold on;
plot(1:n_measure, arr_SA(:,1));
plot(1:n_measure, arr_LS(:,1));
plot(1:n_measure, arr_EV(:,1));
title('Exercise a): Student 1 $\hat{R}_{*}(N)$', ...
'Interpreter', 'latex', 'FontSize', 14);
xlabel('Measurement');
ylabel('R / \Omega');
legend('$\hat{R}_{SA}$', '$\hat{R}_{LS}$', '$\hat{R}_{EV}$');
set(legend, 'Interpreter', 'latex', 'FontSize', 14);

end

